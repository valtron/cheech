from io import StringIO

from cheech.specfile import parse
from cheech.codegen import PythonCodegen

class TestAll:
	def setup_method(self, method):
		self.fs = Fs()
	
	def test_slash1(self):
		tokcls = _create_tokenizer('tests/slash1.txt')
		self._test_slash_common(tokcls)
	
	def test_slash2(self):
		tokcls = _create_tokenizer('tests/slash2.txt')
		self._test_slash_common(tokcls)
	
	def _test_slash_common(self, tokcls):
		for n in range(1, 5 + 1):
			(q, r) = divmod(n, 2)
			expected = [('//', '//')] * q
			if r:
				expected.append(('/', '/'))
			
			self.assertTokenizes(tokcls, '/' * n, expected)
	
	def test_complex(self):
		tokcls = _create_tokenizer('tests/complex.txt')
		
		self.assertTokenizes(tokcls,
			'abc += 123/$ 21 #hello #2 + // comment\n"he\\"',
			[
				('id', 'abc'), ('ws', ' '),
				('+=', '+='), ('ws', ' '),
				('num', '123'), ('/', '/'), ('ERR', '$'), ('ws', ' '),
				('num', '21'), ('ws', ' '),
				('pnd', '#hello'), ('ws', ' '),
				('ERR', '#'), ('num', '2'), ('ws', ' '),
				('+', '+'), ('ws', ' '), ('slc', '// comment'), ('ws', '\n'),
				('ERR', '"he\\"'),
			]
		)
	
	def assertTokenizes(self, tokcls, txt, expected):
		toker = tokcls(txt, self.fs)
		
		idx = 0
		while toker.moveNext():
			(tt, i, j, txt) = toker.current
			pair = (tt, txt)
			
			assert idx < len(expected), "Expected no more tokens, got {}".format(pair)
			
			exp = expected[idx]
			
			assert exp == pair, "Expected {}, got {}".format(exp, pair)
			
			idx += 1
		
		assert idx >= len(expected), "Expected {}, got no more tokens".format(expected[idx])

class Fs(object):
	def a(self, c):
		return c.isalpha()
	
	def n(self, c):
		return c.isdigit()
	
	def an(self, c):
		return c.isalnum()
	
	def ws(self, c):
		return c.isspace()
	
	def nnl(self, c):
		return c and c != '\n'

def _create_tokenizer(filename):
	states = parse(filename)
	out = StringIO()
	clsname = 'Toker'
	PythonCodegen(out, states).generate(clsname)
	return _compile_class(out.getvalue(), clsname)

def _compile_class(code, name):
	env = {}
	exec(compile(code, '<string>', 'single'), env)
	return env[name]
