# cheech

A very simple, hackable scanner generator. Currently in alpha.

## Lexer Spec File

Files are made of states. Lexing starts in the `START` state. The first line is the name of the state. Indented lines are rules for current state. Each rule is an action, followed by zero or more reps. A rep is a character (enclosed in single quotes) or character class name (defined externally) followed by a repeater: `+`, `*`, `?`, or blank (meaning exactly once).

Actions either go to a state, or emit a token. After a token is emitted, the lexer always returns to `START`. If the action emits a token, but the rule has no reps, reps are created based on the name of the token.

Sample file, showing how to parse strings and some random tokens:

```
# state 'START'
START
	# go to state 'str'
	str   '"'
	
	# emit a token named '+='
	# this syntactic sugar, equivalent to
	#:+=   '+' '='
	:+=
	
	# emit a token named 'id'
	# 'a' and 'an' are character classes; how they're defined and used
	# is determined by your codegen; see PythonCodegen and the tests for an example
	:id   a an*

# state 'str'
str
	# saw a '"'? emit token named 'str'!
	:str  '"'
	# saw an escaped quote? stay in 'str'!
	str   '\' '"'
	# saw something from the class 'any'? stay in 'str'!
	str   any
```
