import re

from .algos import optimize_states, State, Item, Action, Rep, Term

def parse(filename):
	parser = CheechFileParser()
	
	with open(filename, 'r') as fh:
		parser.parse(fh)
	
	return optimize_states(parser.states)

class CheechFileParser(object):
	def __init__(self):
		self.states = {}
		self.curstate = None
		
		self._add_state('START')
	
	def parse(self, fh):
		for line in fh:
			indented = line.startswith('\t')
			line = line.strip()
			
			if not line or line.startswith('#'):
				continue
			
			parts = re.split(r'\s+', line)
			
			if indented:
				self._add_rule(parts[0], parts[1:])
			else:
				self.curstate = self._add_state(parts[0])
	
	def _add_rule(self, head, tail):
		if head[0] == ':':
			tok = head[1:]
			goto = 'START'
		else:
			tok = None
			goto = head
		
		if tail:
			parts = list(map(Rep.Parse, tail))
		elif tok:
			parts = list(map(lambda c: Rep(
				Term('c', c), '1'
			), tok))
		else:
			parts = []
		
		item = Item(parts, 0)
		self._add_edge(self.curstate, item, Action(tok, goto))
	
	def _add_edge(self, st, item, action):
		for it in Item.Complete(item):
			r = it.expecting()
			
			if not r:
				t = None
				st.add_edge(t, action)
				continue
			
			t = r.term
			
			if r.rep == '*':
				nst = st
			else:
				if t in st.edges:
					nst = self.states[st.edges[t].goto]
				else:
					nst = self._add_state()
				st.add_edge(t, Action(None, nst.name))
			
			if r.rep in ('*', '+'):
				nst.add_edge(t, Action(None, nst.name))
			
			self._add_edge(nst, it.next(), action)
	
	def _add_state(self, name = None):
		id = len(self.states)
		
		if not name:
			name = '${}'.format(id)
		
		if name not in self.states:
			st = State(name, id)
			self.states[name] = st
		
		return self.states[name]
