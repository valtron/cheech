from collections import OrderedDict

class Item(object):
	def __init__(self, rule, i):
		self.rule = rule
		self.i = i
	
	@classmethod
	def Complete(cls, item):
		yield item
		
		i = item.i
		rule = item.rule
		while i < len(rule):
			if rule[i].rep in ('1', '+'):
				break
			i += 1
			yield cls(item.rule, i)
	
	def expecting(self):
		if self.i < len(self.rule):
			return self.rule[self.i]
		return None
	
	def next(self):
		if self.i >= len(self.rule):
			raise Exception()
		return Item(self.rule, self.i + 1)
	
	def __repr__(self):
		r = self.rule
		i = self.i
		return '[{} . {}]'.format(
			' '.join(map(repr, r[:i])),
			' '.join(map(repr, r[i:])),
		)

class State(object):
	START = 0
	
	def __init__(self, name, id):
		self.name = name
		self.id = id
		self.edges = OrderedDict()
	
	def add_edge(self, term, action):
		self.edges[term] = action
	
	def __repr__(self):
		s = '{} {}\n'.format(self.name, self.id)
		for t, a in self.edges.items():
			s += '\t{!r} {!r}\n'.format(t, a)
		return s

class Action(object):
	def __init__(self, redux, goto):
		self.redux = redux
		self.goto = goto
	
	def __repr__(self):
		return 'r:{} g:{}'.format(self.redux, self.goto)

class Rep(object):
	@classmethod
	def Parse(cls, s):
		l = s[-1:]
		
		if l in ('?', '*', '+') and len(s) > 1:
			rep = l
			s = s[:-1]
		else:
			rep = '1'
		
		return cls(Term.Parse(s), rep)
	
	def __init__(self, term, rep):
		self.term = term
		self.rep = rep
	
	def __repr__(self):
		return '<{!r}{}>'.format(self.term, self.rep)

class Term(object):
	@classmethod
	def Parse(cls, s):
		if s[:1] == '!':
			neg = True
			s = s[1:]
		else:
			neg = False
		
		if s[:1] in ('\'', '"'):
			type = 'c'
			s = s[1:-1]
		else:
			type = 'f'
		
		return cls(type, s)
	
	def __init__(self, type, name, neg = False):
		self.type = type
		self.name = name
		self.neg = neg
	
	def __eq__(self, other):
		return self.__tuple__() == other.__tuple__()
	
	def __hash__(self):
		return hash(self.__tuple__())
	
	def __tuple__(self):
		return (self.type, self.name, self.neg)
	
	def __repr__(self):
		return '{}:{}'.format(self.type, self.name)

def optimize_states(states):
	for st in states.values():
		_optimize_pass(states, st)
	
	reachable = set()
	_find_reachable(states, 'START', reachable)
	
	return {
		name: states[name] for name in reachable
	}

def _optimize_pass(states, s1):
	# Very simple optimization:
	# If there is an edge e1 to a state s2 whose only
	# outgoing edge e2 is an epsilon-transition to s3, and if
	# NOT both e1 and e2 have a reduce action,
	# we can skip s2 and go directly to s3.
	
	for act1 in s1.edges.values():
		s2 = states[act1.goto]
		
		act2 = s2.edges.get(None)
		
		if act2 is None or len(s2.edges) > 1:
			continue
		
		r1 = act1.redux
		r2 = act2.redux
		
		if not (r1 is None or r2 is None):
			continue
		
		act1.redux = (r1 or r2)
		act1.goto = act2.goto

def _find_reachable(states, name, reachable):
	if name in reachable:
		return
	
	reachable.add(name)
	
	for act in states[name].edges.values():
		_find_reachable(states, act.goto, reachable)
