from .algos import State

class Codegen(object):
	def __init__(self, fh):
		self._idl = 0
		self._id = '\t' * self._idl
		self.fh = fh
	
	def blank(self):
		self.line('')
	
	def line(self, txt, *args, **kwargs):
		self.fh.write(self._id)
		self.fh.write(txt.format(*args, **kwargs))
		self.fh.write('\n')
	
	def indent(self):
		self._indent(+1)
	
	def dedent(self):
		self._indent(-1)
	
	def _indent(self, n):
		self._idl += n
		self._id = '\t' * self._idl

class PythonCodegen(Codegen):
	def __init__(self, fh, states):
		assert len(states) > 0
		
		super().__init__(fh)
		# Map[state name, state]
		self.state_by_name = states
		# Map[state ids (sid), consecutive ints (cid)]
		self.state_id_map = dict(
			(sid, cid) for (cid, sid) in enumerate(sorted([st.id for st in states.values() ]))
		)
		# Map[cid, state]
		self.state_by_cid = { self.state_id_map[st.id]: st for st in states.values() }
	
	def generate(self, clsname):
		self.begin('class {}', clsname)
		self._ctor()
		self._moveNext()
		self._current()
		self._step()
		self.end()
	
	def _ctor(self):
		self.begin('def __init__(self, txt, fs)')
		self.line('self.txt = txt')
		self.line('self.fs = fs')
		self.line('self.i = 0')
		self.line('self.j = -1')
		self.line('self.s = {!r}', self.state_id_map[State.START])
		self.line('self.t = None')
		self.line('self._step()')
		self.end()
	
	def _moveNext(self):
		self.begin('def moveNext(self)')
		self.begin('while True')
		self.line('s = self.s')
		self._moveNext_tree(0, len(self.state_by_name))
		self.end()
		self.end()
	
	def _moveNext_tree(self, a, b):
		if b - a < 2:
			self._moveNext_state(self.state_by_cid[a])
			return
		
		(la, lb), (ra, rb) = split_range(a, b)
		
		self.begin('if s < {!r}', ra)
		self._moveNext_tree(la, lb)
		self.end()
		self.begin('else')
		self._moveNext_tree(ra, rb)
		self.end()
	
	def _moveNext_state(self, st):
		had_else = False
		
		sorted_edges = sorted(st.edges.items(), key = lambda e: e[0] is None)
		
		for term, action in sorted_edges:
			if term:
				self.begin('if {}', self._cond(term))
			else:
				had_else = True
			
			sp = self.state_by_name[action.goto]
			
			if term:
				self.line('self._step()')
			
			if action.redux:
				self._emit(action.redux)
				self.line('self.s = {!r}', self.state_id_map[sp.id])
				self.line('return True')
			else:
				self.line('self.s = {!r}', self.state_id_map[sp.id])
			
			if term:
				self.line('continue')
				self.end()
		
		if st.id == State.START:
			self.begin('if self.c == \'\'')
			self.begin('if self.j > self.i')
			self._emit('ERR')
			self.line('return True')
			self.end()
			self.line('return False')
			self.end()
			
			self.line('self._step()')
			self._emit('ERR')
			self.line('return True')
		elif not had_else:
			self._emit('ERR')
			self.line('self.s = {!r}', self.state_id_map[State.START])
			self.line('return True')
	
	def _emit(self, tok):
		self.line('self.t = ({!r}, self.i, self.j, self.txt[self.i:self.j])', tok)
		self.line('self.i = self.j')
	
	def _current(self):
		self.line('@property')
		self.begin('def current(self)')
		self.line('return self.t')
		self.end()
	
	def _step(self):
		self.begin('def _step(self)')
		self.line('self.j += 1')
		self.line('self.c = self.txt[self.j] if self.j < len(self.txt) else \'\'')
		self.end()
	
	def begin(self, line, *args, **kwargs):
		self.line(line + ':', *args, **kwargs)
		self.indent()
	
	def end(self):
		self.dedent()
		self.blank()
	
	def _cond(self, term):
		if term.type == 'c':
			return 'self.c == {}'.format(repr(term.name))
		
		if term.name == '*':
			return 'True'
		
		cond = 'self.fs.{}(self.c)'.format(term.name)
		
		if term.neg:
			cond = 'not ({})'.format(cond)
		
		return cond

def split_range(a, b):
	assert b - a > 1
	h = (a + b) // 2
	return (a, h), (h, b)
